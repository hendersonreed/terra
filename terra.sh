# This is a simple command-line client for the Terra API

set -e

SERVER=https://terra.finzdani.net/api
AUTH_FILE=~/.terra_auth

if [[ -f $AUTH_FILE ]] ; then
    TOKEN=$(cat $AUTH_FILE)
fi

help() {
cat << EOM
the terra CLI client.

Usage:
    terra.sh COMMAND

Commands:
    sites                     View all sites as JSON. jq is recommended
    register <code>           Register a new account using the given creation code
    login                     Login and authenticate all other commands
    new_code                  Create a new registration code
    add                       Add a new JSON-encoded site
    get        <id>           Get a site from the database by id
    remove     <id>           Remove a site from the database
    update     <id>           Update a site by providing a new JSON-encoded entry
    add_tag    <id> <tag>     Add a new tag to a site
    remove_tag <id> <tag>     Remove a tag from a site
    help                      Show this screen

SITE:
    { 
      "url": String,
      "title": String,
      "description: String,
      "tags": [String]
    }
EOM
}

prompt() {
    read -rp "Username: " user 
    read -rsp "Password: " pass 
}

command=$1

case $command in 
    "help"|"--help")
        help
        ;;
    "sites")
        curl -sS "$SERVER/sites"
        echo
        ;;
    "register")
        prompt
        curl -sSH "Content-Type: application/json" "$SERVER/register/$2" -d "{\"username\": \"$user\", \"password\": \"$pass\"}"
        echo
        ;;
    "login")
        prompt
        token=$(curl -fsS -H "Content-Type: application/json" "$SERVER/auth" -d "{\"username\": \"$user\", \"password\": \"$pass\"}")
        echo "$token" > $AUTH_FILE
        chmod 600 $AUTH_FILE
        echo "Login succesful"
        ;;
    "new_code")
        curl -sSH "X-AUTH: $TOKEN" $SERVER/new_code
        echo
        ;;
    "add")
        TEMP=$(mktemp /tmp/terra.new_site.XXXXXX)
        cat << EOF > "$TEMP"
{ 
  "url": "",
  "title": "",
  "description": "",
  "tags": ["",""]
}
EOF
        $EDITOR "$TEMP"
        curl -sSH "X-AUTH: $TOKEN" -H "Content-Type: application/json" $SERVER/add_site -d @"$TEMP"
        echo
        rm "$TEMP"
        ;;
    "get")
        curl -sS $SERVER/site/"$2"
        echo
        ;;
    "remove")
        curl -sSH "X-AUTH: $TOKEN" $SERVER/site/"$2" -X DELETE
        echo
        ;;
    "update")
        TEMP=$(mktemp /tmp/terra.update_site.XXXXXX)
        curl $SERVER/site/"$2" | jq '{ url, description, title, tags }' > "$TEMP"
        echo
        $EDITOR "$TEMP"
        curl -sSH "X-AUTH: $TOKEN" -H "Content-Type: application/json" -X PUT $SERVER/site/"$2" -d @"$TEMP"
        ;;
    "add_tag")
        curl -sSH "X-AUTH: $TOKEN" -X POST $SERVER/site/"$2"/tags/add/"$3"
        echo
        ;;
    "remove_tag")
        curl -sSH "X-AUTH: $TOKEN" -X POST $SERVER/site/"$2"/tags/remove/"$3"
        echo
        ;;
    # If we didn't get any valid commands just show the help entry
    # and the given command
    *)
        echo "Invalid command: $command"
        help
        ;;

esac

