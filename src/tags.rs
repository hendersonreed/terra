//    terra, a human-focused web directory
//    Copyright (C) 2020 Waylon Cude
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published
//    by the Free Software Foundation, version 3 of the License.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

use rocket::request::FromSegments;
use rocket::http::uri::Segments;

pub struct Tags(pub Vec<String>);

impl<'a> FromSegments<'a> for Tags {
    type Error = !;

    fn from_segments(segments: Segments<'a>) -> Result<Self, Self::Error> {
        let mut tags = Vec::new();
        for segment in segments {
            tags.push(segment.to_string())
        }
        Ok(Tags(tags))
    }
}

impl Tags {
    pub fn contains(&self, tag: &str) -> bool {
        for t in self.0.iter() {
            if t == tag {
                return true;
            }
        }
        return false;
    }
}
