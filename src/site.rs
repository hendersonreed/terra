//    terra, a human-focused web directory
//    Copyright (C) 2020 Waylon Cude
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published
//    by the Free Software Foundation, version 3 of the License.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

use serde::{Deserialize,Serialize};
use crate::db::sites;
use chrono::prelude::*;

// There's a way to reduce duplication with macro_rules but I'm too inebriated and it's probably
// not worth it anyways
#[derive(Deserialize)]
pub struct OptionalPostSite {
    pub url: Option<String>,
    pub description: Option<String>,
    pub title: Option<String>,
    pub tags: Option<Vec<String>>
}
#[derive(Deserialize)]
pub struct PostSite {
    pub url: String,
    pub description: String,
    pub title: String,
    pub tags: Vec<String>
}
impl PostSite {
    pub fn to_db(mut self, uploader: String ) -> Site {
        for tag in self.tags.iter_mut() {
            *tag=tag.to_lowercase();
        }
        let PostSite { url, description, title, tags } = self;
        Site {
            url,
            title,
            description,
            uploader,
            tags,
            post_date: Utc::now().naive_utc(),
            id: None
        }
    }
}
#[derive(Serialize,Queryable,Insertable,AsChangeset)]
pub struct Site {
    pub id: Option<i32>,
    pub url: String,
    pub title: String,
    pub description: String,
    pub uploader: String,
    pub tags: Vec<String>,
    pub post_date: NaiveDateTime
}
