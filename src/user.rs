//    terra, a human-focused web directory
//    Copyright (C) 2020 Waylon Cude
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published
//    by the Free Software Foundation, version 3 of the License.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <https://www.gnu.org/licenses/>.

use serde::Deserialize;
use bcrypt::BcryptError;
use crate::api;
use crate::token::Token;
use crate::db::users;

#[derive(Deserialize,Queryable,Insertable)]
pub struct User {
    pub username: String,
    pub password: String,
    pub tokens: Vec<Token>
}

#[derive(Deserialize)]
pub struct PostUser {
    pub username: String,
    pub password: String,
}
impl PostUser {
    pub fn to_db(self) -> Result<User,BcryptError> {
        Ok(User {
            username: self.username,
            password: api::hash(&self.password)?,
            tokens: Vec::new()
        })
    }
}
